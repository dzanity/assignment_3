package controller

import (
	"assignment_3/config"
	"assignment_3/model/api"
	"net/http"

	"github.com/gin-gonic/gin"
)

func GetWeather(c *gin.Context) {
	data := config.Read()

	apiResponse := api.ApiResponse{
		Code:    http.StatusOK,
		Message: "OK",
		Data:    data,
	}

	c.JSON(apiResponse.Code, apiResponse)
}
