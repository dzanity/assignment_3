package config

import (
	"assignment_3/model/entity"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
)

func Read() interface{} {
	jsonFile, err := os.Open("config/status.json")
	if err != nil {
		log.Fatal("Error loading .json file")
	}

	fmt.Println("Successfully open config/status.json")
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)

	var status entity.Weather

	json.Unmarshal(byteValue, &status)

	return status
}

func Modify() interface{} {
	jsonFile, err := os.Open("config/status.json")
	if err != nil {
		log.Fatal("Error loading .json file")
	}

	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)

	var status entity.Weather

	json.Unmarshal(byteValue, &status)

	min := 1
	max := 100

	status.Water = rand.Intn(max-min) + min
	status.Wind = rand.Intn(max-min) + min

	switch {
	case status.Water <= 5:
		status.WaterStatus = "Safe"
	case status.Water <= 8:
		status.WaterStatus = "Standby"
	case status.Water > 8:
		status.WaterStatus = "Danger"
	default:
		status.WaterStatus = "Initiate API"
	}

	switch {
	case status.Wind <= 6:
		status.WindStatus = "Safe"
	case status.Wind <= 7:
		status.WindStatus = "Standby"
	case status.Wind < 15:
		status.WindStatus = "Standby"
	case status.Wind > 15:
		status.WindStatus = "Danger"
	default:
		status.WindStatus = "Initiate API"
	}

	byteValue, err = json.Marshal(status)
	if err != nil {
		return err
	}

	err = ioutil.WriteFile("config/status.json", byteValue, 0644)
	if err != nil {
		fmt.Println("Failed updating status.json")
	}

	fmt.Println("Successfully updating status.json")

	return nil
}
