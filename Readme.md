# Assignment-3

## Link gitlab backend: https://gitlab.com/dzanity/assignment_3
## Link gitlab frontend: https://gitlab.com/dzanity/assignment_3_frontend

- Build .env based on your environment
- Run go run main.go

Dependencies:
- GODOTENV: "github.com/joho/godotenv"
- GIN GONIC: "github.com/gin-gonic/gin"
- CRON: "github.com/robfig/cron/v3"